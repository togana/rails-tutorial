# Ruby on Rails チュートリアルのサンプルアプリケーション

これは、次の教材で作られたサンプルアプリケーションです。   
[*Ruby on Rails チュートリアル: 実例を使って Rails を学ぼう*](http://railstutorial.jp/)
[Michael Hartl](http://www.michaelhartl.com/) 著

## ライセンス

[Ruby on Rails チュートリアル](http://railstutorial.jp/)内にあるすべてのソースコードは
MIT ライセンスと Beerware ライセンスのもとに公開されています。
詳細は [LICENSE](https://railstutorial.jp/chapters/beginning?version=5.0#copyright) をご覧ください。

## 実行する前に

デベロップ環境で動かすことのみを想定しています。プロダクション等の設定はデフォルトのままです。
開発を用意に始めるためにもともとのサンプルにはないdockerで環境を用意しています。
テストは1週目は省いて試して見ても良いようだったので省きました。次の機会で振り返りながら導入しようと思っています。

## 必要なツール

下記のツール群がインストールできていればOKです。

- Docker
- Docker Machine
- Docker Compose
- VirtualBox

Macの場合はbrewコマンドですべてインストールできます。  
docker for macでも問題はないと思いますが検証していません。

## 使い方

このアプリケーションを動かす場合は、まずはリポジトリを手元にクローンしてください。

その後、次のコマンドで Docker Host を作成します。

```
$ docker-machine create -d virtualbox rails-tutorial
```

その後、 `.env` ファイルが必要になるので `.env.sample` をコピーして  
下記で取得したIPを APP_HOST に記載してください。

```
$ docker-machine ip rails-tutorial
```

その後、次のコマンドで 環境変数 を追加します(windowsの場合はコマンドが違います、適宜読み替えてください)。

```
$ docker-machine env rails-tutorial
$ eval $(docker-machine env rails-tutorial)
```

その後、次のコマンドで アプリ をビルドします。

```
$ docker-compose build
```

その後、次のコマンドで アプリ を実行します。

```
$ docker-compose up -d
```


その後、データベースへのマイグレーションを実行します。

```
$ docker-compose exec app rails db:migrate
```

その後、データベースに初期データを入れたい場合は下記コマンドを実行します。

```
$ docker-compose exec app rails db:seed
```

メール周りはMailCatcherをあわせて導入しているので  
下記コマンドでブラウザが立ち上がり確認することができます。

```
$ open http://$(docker-machine ip rails-tutorial):1080
```

詳しくは、[*Ruby on Rails チュートリアル*](http://railstutorial.jp/)を参考にしてください。
