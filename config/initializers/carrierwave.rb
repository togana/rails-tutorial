CarrierWave.configure do |config|
  config.fog_provider = 'fog/aws'
  config.fog_credentials = {
    provider: 'AWS',
    aws_access_key_id: ENV['MINIO_ACCESS_KEY'],
    aws_secret_access_key: ENV['MINIO_SECRET_KEY'],
    endpoint: "http://#{ENV['APP_HOST']}:9000",
    path_style: true,
  }

  config.fog_directory = 'tutorial'
  CarrierWave::SanitizedFile.sanitize_regexp = /[^[:word:]\.\-\+]/
end
